*SPICE netlist created from verilog structural netlist module uart by vlog2Spice (qflow)
*This file may contain array delimiters, not for use in simulation.

** Start of included library /usr/local/share/qflow/tech/osu050/osu050_stdcells.sp

.subckt AND2X1 Y B vdd gnd A
M0 a_2_6# A vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B a_2_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y a_2_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_9_6# A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 gnd B a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y a_2_6# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends AND2X1

.subckt AND2X2 vdd gnd A B Y
M0 a_2_6# A vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B a_2_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y a_2_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_9_6# A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 gnd B a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y a_2_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends AND2X2

.subckt AOI21X1 gnd vdd A B Y C
M0 vdd A a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_2_54# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y C a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_12_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 Y B a_12_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd C Y gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends AOI21X1

.subckt AOI22X1 gnd vdd C D Y A B
M0 vdd A a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_2_54# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y D a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_2_54# C Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_11_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y B a_11_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_28_6# D Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 gnd C a_28_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends AOI22X1

.subckt BUFX2 vdd gnd A Y
M0 vdd A a_2_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y a_2_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 gnd A a_2_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 Y a_2_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends BUFX2

.subckt BUFX4 vdd gnd A Y
M0 vdd A a_2_6# vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y a_2_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd a_2_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 gnd A a_2_6# gnd nfet w=4.5u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 Y a_2_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd a_2_6# Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends BUFX4

.subckt CLKBUF1 A vdd gnd Y
M0 a_9_6# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd A a_9_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_25_6# a_9_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd a_9_6# a_25_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_41_6# a_25_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 vdd a_25_6# a_41_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Y a_41_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 vdd a_41_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_9_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 gnd A a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 a_25_6# a_9_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 gnd a_9_6# a_25_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_41_6# a_25_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 gnd a_25_6# a_41_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 Y a_41_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 gnd a_41_6# Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends CLKBUF1

.subckt CLKBUF2 vdd gnd A Y
M0 a_9_6# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd A a_9_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_25_6# a_9_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd a_9_6# a_25_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_41_6# a_25_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 vdd a_25_6# a_41_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_57_6# a_41_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 vdd a_41_6# a_57_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_73_6# a_57_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 vdd a_57_6# a_73_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 Y a_73_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 vdd a_73_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_9_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 gnd A a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 a_25_6# a_9_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 gnd a_9_6# a_25_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_41_6# a_25_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 gnd a_25_6# a_41_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 a_57_6# a_41_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 gnd a_41_6# a_57_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 a_73_6# a_57_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 gnd a_57_6# a_73_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 Y a_73_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 gnd a_73_6# Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends CLKBUF2

.subckt CLKBUF3 gnd vdd A Y
M0 a_9_6# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd A a_9_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_25_6# a_9_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd a_9_6# a_25_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_41_6# a_25_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 vdd a_25_6# a_41_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_57_6# a_41_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 vdd a_41_6# a_57_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_73_6# a_57_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 vdd a_57_6# a_73_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 a_89_6# a_73_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 vdd a_73_6# a_89_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_105_6# a_89_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 vdd a_89_6# a_105_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 Y a_105_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 vdd a_105_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_9_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 gnd A a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 a_25_6# a_9_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 gnd a_9_6# a_25_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 a_41_6# a_25_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 gnd a_25_6# a_41_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 a_57_6# a_41_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 gnd a_41_6# a_57_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_73_6# a_57_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 gnd a_57_6# a_73_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 a_89_6# a_73_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 gnd a_73_6# a_89_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M28 a_105_6# a_89_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M29 gnd a_89_6# a_105_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M30 Y a_105_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M31 gnd a_105_6# Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends CLKBUF3

.subckt DFFNEGX1 CLK vdd D gnd Q
M0 vdd CLK a_2_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_17_74# D vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_23_6# a_2_6# a_17_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_31_74# CLK a_23_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd a_34_4# a_31_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_34_4# a_23_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_61_74# a_34_4# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_66_6# CLK a_61_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_76_84# a_2_6# a_66_6# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 vdd Q a_76_84# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 gnd CLK a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 Q a_66_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_17_6# D gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_23_6# CLK a_17_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 a_31_6# a_2_6# a_23_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 gnd a_34_4# a_31_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_34_4# a_23_6# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_61_6# a_34_4# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 a_66_6# a_2_6# a_61_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_76_6# CLK a_66_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd Q a_76_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 Q a_66_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends DFFNEGX1

.subckt DFFPOSX1 vdd D gnd Q CLK
M0 vdd CLK a_2_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_17_74# D vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_22_6# CLK a_17_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_31_74# a_2_6# a_22_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd a_34_4# a_31_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_34_4# a_22_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_61_74# a_34_4# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_66_6# a_2_6# a_61_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_76_84# CLK a_66_6# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 vdd Q a_76_84# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 gnd CLK a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 Q a_66_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_17_6# D gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_22_6# a_2_6# a_17_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 a_31_6# CLK a_22_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 gnd a_34_4# a_31_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_34_4# a_22_6# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_61_6# a_34_4# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 a_66_6# CLK a_61_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_76_6# a_2_6# a_66_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd Q a_76_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 Q a_66_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends DFFPOSX1

.subckt DFFSR gnd vdd D S R Q CLK
M0 a_2_6# R vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd a_10_61# a_2_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_10_61# a_23_27# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd S a_10_61# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_23_27# a_47_71# a_2_6# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_57_6# a_47_4# a_23_27# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 vdd D a_57_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 vdd a_47_71# a_47_4# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_47_71# CLK vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_105_6# a_47_71# a_10_61# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 a_113_6# a_47_4# a_105_6# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 a_122_6# a_105_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 vdd R a_122_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_113_6# a_122_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 vdd S a_113_6# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 vdd a_122_6# Q vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_10_6# R a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 gnd a_10_61# a_10_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 a_26_6# a_23_27# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_10_61# S a_26_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 a_23_27# a_47_4# a_2_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 a_57_6# a_47_71# a_23_27# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 gnd D a_57_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 gnd a_47_71# a_47_4# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_47_71# CLK gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 a_105_6# a_47_4# a_10_61# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 a_113_6# a_47_71# a_105_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 a_130_6# a_105_6# a_122_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M28 gnd R a_130_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M29 a_146_6# a_122_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M30 a_113_6# S a_146_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M31 gnd a_122_6# Q gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends DFFSR

.subckt FAX1 gnd vdd A B C YC YS
M0 vdd A a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_2_54# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_25_6# C a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_33_54# B a_25_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd A a_33_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_46_54# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 vdd B a_46_54# vdd pfet w=10.8u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_46_54# C vdd vdd pfet w=10.8u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_70_6# a_25_6# a_46_54# vdd pfet w=10.8u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_79_46# C a_70_6# vdd pfet w=14.4u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 a_84_46# B a_79_46# vdd pfet w=14.4u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 vdd A a_84_46# vdd pfet w=14.4u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 YS a_70_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 YC a_25_6# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 gnd A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 a_2_6# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 a_25_6# C a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_33_6# B a_25_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 gnd A a_33_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_46_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd B a_46_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 a_46_6# C gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 a_70_6# a_25_6# a_46_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 a_79_6# C a_70_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_84_6# B a_79_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 gnd A a_84_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 YS a_70_6# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 YC a_25_6# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends FAX1

.subckt FILL vdd gnd
.ends FILL

.subckt HAX1 vdd gnd YC A B YS
M0 vdd A a_2_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_2_74# B vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd a_2_74# YC vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_41_74# a_2_74# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_49_54# B a_41_74# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 vdd A a_49_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 YS a_41_74# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_9_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_2_74# B a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 gnd a_2_74# YC gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 a_38_6# a_2_74# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 a_41_74# B a_38_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M12 a_38_6# A a_41_74# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 YS a_41_74# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends HAX1

.subckt INVX1 A Y vdd gnd
M0 Y A vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y A gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends INVX1

.subckt INVX2 vdd gnd Y A
M0 Y A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends INVX2

.subckt INVX4 vdd gnd Y A
M0 Y A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd A Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 gnd A Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends INVX4

.subckt INVX8 vdd gnd A Y
M0 Y A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd A Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd A Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 Y A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd A Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Y A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 gnd A Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends INVX8

.subckt LATCH D Q gnd vdd CLK
M0 vdd CLK a_2_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_18_74# D vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_23_6# a_2_6# a_18_74# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_35_84# CLK a_23_6# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd Q a_35_84# vdd pfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd CLK a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Q a_23_6# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_18_6# D gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_23_6# CLK a_18_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_35_6# a_2_6# a_23_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 gnd Q a_35_6# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 Q a_23_6# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends LATCH

.subckt MUX2X1 S vdd gnd Y A B
M0 vdd S a_2_10# vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_17_50# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y S a_17_50# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_30_54# a_2_10# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd A a_30_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd S a_2_10# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 a_17_10# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 Y a_2_10# a_17_10# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 a_30_10# S Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 gnd A a_30_10# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends MUX2X1

.subckt NAND2X1 vdd Y gnd A B
M0 Y A vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B Y vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_9_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 Y B a_9_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends NAND2X1

.subckt NAND3X1 B vdd gnd A C Y
M0 Y A vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B Y vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y C vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_9_6# A gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_14_6# B a_9_6# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y C a_14_6# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends NAND3X1

.subckt NOR2X1 vdd B gnd Y A
M0 a_9_54# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y B a_9_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y A gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 gnd B Y gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends NOR2X1

.subckt NOR3X1 vdd gnd B C A Y
M0 vdd A a_2_64# vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_2_64# A vdd vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_25_64# B a_2_64# vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_2_64# B a_25_64# vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 Y C a_25_64# vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_25_64# C Y vdd pfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Y A gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 gnd B Y gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 Y C gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends NOR3X1

.subckt OAI21X1 gnd vdd A B Y C
M0 a_9_54# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y B a_9_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd C Y vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 gnd A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_2_6# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y C a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends OAI21X1

.subckt OAI22X1 gnd vdd D C A B Y
M0 a_9_54# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y B a_9_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_28_54# D Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd C a_28_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 gnd A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_2_6# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Y D a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_2_6# C Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends OAI22X1

.subckt OR2X1 Y B vdd gnd A
M0 a_9_54# A a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B a_9_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y a_2_54# vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_2_54# A gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 gnd B a_2_54# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y a_2_54# gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends OR2X1

.subckt OR2X2 Y B vdd gnd A
M0 a_9_54# A a_2_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 vdd B a_9_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y a_2_54# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_2_54# A gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 gnd B a_2_54# gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 Y a_2_54# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends OR2X2

.subckt PADINC DI vdd2 gnd vdd gnd2 YPAD
M0 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M1 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M3 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M5 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M6 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M7 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M8 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M9 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M10 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M11 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M12 gnd gnd2 a_13_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_29_269# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 gnd gnd2 a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 a_31_41# gnd2 gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 gnd gnd2 a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_31_41# gnd2 gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 gnd gnd2 a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M28 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M29 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M30 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M31 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M32 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M33 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M34 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M35 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M36 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M37 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M38 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M39 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M40 vdd gnd2 a_13_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M41 a_29_269# a_13_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M42 vdd gnd2 a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M43 a_31_451# gnd2 vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M44 vdd gnd2 a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M45 a_31_451# gnd2 vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M46 vdd gnd2 a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M47 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M48 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M49 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M50 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M51 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M52 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M53 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M54 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M55 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M56 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M57 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M58 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M59 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M60 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M61 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M62 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M63 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M64 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M65 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M66 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M67 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M68 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M69 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M70 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M71 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M72 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M73 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M74 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M75 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M76 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M77 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M78 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M79 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
R0 YPAD a_191_395# 100
R1 a_191_395# YPAD 100
.ends PADINC

.subckt PADINOUT DO DI OEN gnd2 vdd2 vdd gnd YPAD
M0 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M1 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M3 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M5 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M6 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M7 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M8 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M9 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M10 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M11 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M12 gnd OEN a_13_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_29_269# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 a_31_41# DO gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_31_41# DO gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M28 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M29 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M30 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M31 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M32 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M33 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M34 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M35 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M36 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M37 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M38 DI a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M39 gnd a_200_269# DI gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M40 vdd OEN a_13_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M41 a_29_269# a_13_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M42 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M43 a_31_451# DO vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M44 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M45 a_31_451# DO vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M46 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M47 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M48 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M49 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M50 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M51 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M52 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M53 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M54 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M55 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M56 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M57 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M58 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M59 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M60 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M61 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M62 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M63 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M64 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M65 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M66 DI a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M67 vdd a_200_269# DI vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M68 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M69 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M70 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M71 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M72 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M73 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M74 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M75 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M76 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M77 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M78 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M79 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
R0 YPAD a_191_395# 100
R1 a_191_395# YPAD 100
.ends PADINOUT

.subckt PADOUT DO vdd2 gnd2 vdd gnd YPAD
M0 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M1 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M3 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M5 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M6 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M7 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M8 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M9 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M10 vdd2 a_31_451# YPAD vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M11 YPAD a_31_451# vdd2 vdd2 pfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M12 gnd vdd a_13_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M13 a_29_269# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M14 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M15 a_31_41# DO gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M16 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M17 a_31_41# DO gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M18 gnd DO a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M19 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M20 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M21 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M22 gnd a_13_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M23 a_31_41# a_13_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M24 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M25 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M26 a_31_451# a_29_269# a_31_41# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M27 a_31_41# a_29_269# a_31_451# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M28 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M29 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M30 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M31 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M32 a_200_269# a_191_395# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M33 gnd a_191_395# a_200_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M34 a_248_269# a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M35 gnd a_200_269# a_248_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M36 a_248_269# a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M37 gnd a_200_269# a_248_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M38 a_248_269# a_200_269# gnd gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M39 gnd a_200_269# a_248_269# gnd nfet w=9u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M40 vdd vdd a_13_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M41 a_29_269# a_13_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M42 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M43 a_31_451# DO vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M44 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M45 a_31_451# DO vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M46 vdd DO a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M47 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M48 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M49 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M50 vdd a_29_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M51 a_31_451# a_29_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M52 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M53 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M54 a_31_41# a_13_269# a_31_451# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M55 a_31_451# a_13_269# a_31_41# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M56 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M57 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M58 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M59 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M60 a_200_269# a_191_395# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M61 vdd a_191_395# a_200_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M62 a_248_269# a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M63 vdd a_200_269# a_248_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M64 a_248_269# a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M65 vdd a_200_269# a_248_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M66 a_248_269# a_200_269# vdd vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M67 vdd a_200_269# a_248_269# vdd pfet w=15.6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M68 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M69 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M70 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M71 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M72 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M73 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M74 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M75 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M76 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M77 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M78 gnd2 a_31_41# YPAD gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
M79 YPAD a_31_41# gnd2 gnd2 nfet w=30u l=0.9u
+ ad=0p pd=0u as=0p ps=0u 
R0 YPAD a_191_395# 100
R1 a_191_395# YPAD 100
.ends PADOUT

.subckt TBUFX1 vdd gnd EN A Y
M0 a_9_6# EN vdd vdd pfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_26_54# a_9_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 vdd A a_26_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_9_6# EN gnd gnd nfet w=3u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_26_6# EN Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 gnd A a_26_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends TBUFX1

.subckt TBUFX2 vdd gnd A EN Y
M0 a_9_6# EN vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 Y a_9_6# a_18_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 a_18_54# a_9_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 vdd A a_18_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 a_18_54# A vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_9_6# EN gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 Y EN a_18_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_18_6# EN Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 gnd A a_18_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_18_6# A gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends TBUFX2

.subckt XNOR2X1 A B gnd vdd Y
M0 vdd A a_2_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_18_54# a_12_41# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y a_2_6# a_18_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_35_54# A Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd B a_35_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_12_41# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 gnd A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_18_6# a_12_41# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 Y A a_18_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_35_6# a_2_6# Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 gnd B a_35_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 a_12_41# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends XNOR2X1

.subckt XOR2X1 Y vdd B A gnd
M0 vdd A a_2_6# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M1 a_18_54# a_13_43# vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M2 Y A a_18_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M3 a_35_54# a_2_6# Y vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M4 vdd B a_35_54# vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M5 a_13_43# B vdd vdd pfet w=12u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M6 gnd A a_2_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M7 a_18_6# a_13_43# gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M8 Y a_2_6# a_18_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M9 a_35_6# A Y gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M10 gnd B a_35_6# gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
M11 a_13_43# B gnd gnd nfet w=6u l=0.6u
+ ad=0p pd=0u as=0p ps=0u 
.ends XOR2X1

** End of included library /usr/local/share/qflow/tech/osu050/osu050_stdcells.sp

.subckt uart vdd gnd ld_tx_data reset rx_data[0] rx_data[1] rx_data[2]
+ rx_data[3] rx_data[4] rx_data[5] rx_data[6] rx_data[7] rx_empty rx_enable rx_in
+ rxclk tx_data[0] tx_data[1] tx_data[2] tx_data[3] tx_data[4] tx_data[5] tx_data[6]
+ tx_data[7] tx_empty tx_enable tx_out txclk uld_rx_data 

X_397_ vdd gnd _181_[1] rx_data[1] BUFX2
X_321_ gnd vdd tx_cnt[3] _94_ _119_ tx_enable OAI21X1
X_415_ gnd vdd _9_[6] vdd _10__bF$buf3 tx_reg[6] 
+ txclk
+ DFFSR
X_224_ rx_busy _42_ vdd gnd INVX1
X_262_ gnd vdd _72_ _70_ _5_[3] _25_ OAI21X1
X_318_ vdd _116_ gnd tx_cnt[2] _108_ NAND2X1
X_356_ _143_ vdd gnd _127_ _142_ _144_ NAND3X1
X_394_ gnd vdd _176_ _53_ _0_ _62_ AOI21X1
X_259_ rx_enable vdd gnd rx_busy _66_ _71_ NAND3X1
X_297_ vdd _97_ gnd tx_enable _94_ NAND2X1
X_412_ gnd vdd _9_[3] vdd _10__bF$buf0 tx_reg[3] 
+ txclk
+ DFFSR
X_221_ vdd _40_ gnd _39_ _20_ NAND2X1
X_315_ tx_cnt[3] _113_ vdd gnd INVX1
X_353_ _139_ vdd gnd _135_ _140_ _141_ NAND3X1
X_409_ gnd vdd _9_[0] vdd _10__bF$buf3 tx_reg[0] 
+ txclk
+ DFFSR
X_218_ vdd _16_ gnd _37_ rx_cnt[1] NOR2X1
X_391_ gnd vdd _38_ _166_ _175_ rx_reg[4] OAI21X1
X_447_ gnd vdd rx_d1 _10__bF$buf4 vdd rx_d2 
+ rxclk_bF$buf3
+ DFFSR
X_256_ gnd vdd _64_ _44_ _5_[1] _68_ OAI21X1
X_294_ vdd _95_ gnd _93_ _94_ NAND2X1
XFILL41640x24150 vdd gnd FILL
X_388_ gnd vdd _33_ _166_ _173_ rx_reg[5] OAI21X1
X_197_ gnd vdd rx_cnt[2] _17_ _19_ _18_ OAI21X1
X_312_ tx_cnt[2] vdd gnd _94_ _108_ _111_ NAND3X1
X_350_ vdd _137_ gnd _138_ _99_ NOR2X1
X_406_ vdd gnd _184_ tx_out BUFX2
X_215_ vdd _35_ gnd _34_ _20_ NAND2X1
X_444_ gnd vdd _1_[2] vdd _10__bF$buf1 rx_cnt[2] 
+ rxclk_bF$buf4
+ DFFSR
X_253_ _65_ _66_ vdd gnd INVX1
X_309_ vdd _102_ gnd _108_ _93_ NOR2X1
XFILL41880x150 vdd gnd FILL
X_291_ tx_enable _92_ vdd gnd INVX1
X_347_ _121_ vdd gnd _9_[7] _136_ _135_ MUX2X1
X_385_ gnd vdd _59_ _53_ _171_ rx_reg[6] AOI21X1
X_194_ rx_cnt[0] _16_ vdd gnd INVX1
X_288_ _181_[7] _90_ vdd gnd INVX1
X_403_ vdd gnd _181_[7] rx_data[7] BUFX2
X_212_ vdd _15_ gnd _32_ rx_cnt[0] NOR2X1
X_441_ gnd vdd _5_[3] vdd _10__bF$buf4 rx_sample_cnt[3] 
+ rxclk_bF$buf3
+ DFFSR
X_250_ gnd vdd _62_ _61_ _5_[0] rx_busy 
+ _63_
+ AOI22X1
X_306_ gnd vdd _102_ _183_ _106_ _92_ AOI21X1
X_344_ gnd vdd _133_ _121_ _9_[6] _134_ AOI21X1
X_209_ _30_ _25_ vdd gnd _29_ OR2X2
X_382_ gnd vdd _13_ _166_ _169_ rx_reg[7] OAI21X1
X_438_ gnd vdd _5_[0] vdd _10__bF$buf4 rx_sample_cnt[0] 
+ rxclk_bF$buf3
+ DFFSR
X_191_ _12_ vdd gnd _178_ _180_ _13_ NAND3X1
X_247_ rx_sample_cnt[0] _61_ vdd gnd INVX1
X_285_ _181_[6] _88_ vdd gnd INVX1
X_379_ gnd vdd rx_cnt[3] _17_ _166_ _165_ OAI21X1
X_188_ vdd _179_ gnd _180_ rx_sample_cnt[3] NOR2X1
X_400_ vdd gnd _181_[4] rx_data[4] BUFX2
X_303_ vdd _103_ gnd tx_cnt[0] _102_ NAND2X1
X_341_ _121_ vdd gnd _9_[5] _132_ _131_ MUX2X1
X_206_ vdd _27_ gnd _26_ _178_ NAND2X1
XFILL41880x27150 vdd gnd FILL
X_435_ gnd vdd _4_[5] vdd _10__bF$buf5 rx_reg[5] 
+ rxclk_bF$buf2
+ DFFSR
X_244_ vdd _55_ gnd _59_ _23_ NOR2X1
X_282_ _181_[5] _86_ vdd gnd INVX1
X_338_ _121_ vdd gnd _9_[4] _130_ _129_ MUX2X1
X_376_ gnd vdd _184_ _163_ _164_ _101_ AOI21X1
X_185_ vdd gnd _177_ rx_d2 INVX2
XFILL41640x21150 vdd gnd FILL
X_279_ _181_[4] _84_ vdd gnd INVX1
X_300_ _98_ vdd gnd tx_cnt[3] _99_ _100_ NAND3X1
X_203_ gnd vdd _177_ _21_ _4_[3] _24_ OAI21X1
X_432_ gnd vdd _4_[2] vdd _10__bF$buf1 rx_reg[2] 
+ rxclk_bF$buf2
+ DFFSR
X_241_ _56_ vdd gnd _54_ _53_ _57_ NAND3X1
X_335_ _121_ vdd gnd _9_[3] _128_ _127_ MUX2X1
X_373_ vdd gnd _160_ _159_ _97_ _161_ NOR3X1
X_429_ gnd vdd _3_ _10__bF$buf4 vdd _182_ 
+ rxclk_bF$buf4
+ DFFSR
X_238_ vdd _54_ gnd rx_cnt[2] _28_ NAND2X1
X_276_ _181_[3] _82_ vdd gnd INVX1
X_200_ rx_cnt[3] _22_ vdd gnd INVX1
X_332_ gnd vdd _125_ _121_ _9_[2] _126_ AOI21X1
X_370_ vdd tx_cnt[2] gnd _158_ tx_cnt[3] NOR2X1
X_426_ gnd vdd _2_[5] vdd _10__bF$buf1 _181_[5] 
+ rxclk_bF$buf2
+ DFFSR
X_235_ gnd vdd _15_ _46_ _1_[1] _51_ OAI21X1
X_273_ _181_[2] _80_ vdd gnd INVX1
X_329_ _121_ vdd gnd _9_[1] _124_ _123_ MUX2X1
X_367_ gnd vdd _99_ _137_ _155_ _154_ OAI21X1
XBUFX2_insert0 vdd gnd _10_ _10__bF$buf5 BUFX2
XBUFX2_insert1 vdd gnd _10_ _10__bF$buf4 BUFX2
XBUFX2_insert2 vdd gnd _10_ _10__bF$buf3 BUFX2
XBUFX2_insert3 vdd gnd _10_ _10__bF$buf2 BUFX2
XBUFX2_insert4 vdd gnd _10_ _10__bF$buf1 BUFX2
XBUFX2_insert5 vdd gnd _10_ _10__bF$buf0 BUFX2
XBUFX2_insert6 vdd gnd uld_rx_data uld_rx_data_bF$buf3 BUFX2
XBUFX2_insert7 vdd gnd uld_rx_data uld_rx_data_bF$buf2 BUFX2
XBUFX2_insert8 vdd gnd uld_rx_data uld_rx_data_bF$buf1 BUFX2
XBUFX2_insert9 vdd gnd uld_rx_data uld_rx_data_bF$buf0 BUFX2
X_423_ gnd vdd _2_[2] vdd _10__bF$buf1 _181_[2] 
+ rxclk_bF$buf2
+ DFFSR
X_232_ _47_ vdd gnd _16_ _49_ _50_ NAND3X1
X_270_ _181_[1] _78_ vdd gnd INVX1
X_326_ gnd vdd _120_ _121_ _9_[0] _122_ AOI21X1
XFILL41880x24150 vdd gnd FILL
X_364_ gnd vdd _150_ _151_ _152_ _105_ AOI21X1
X_229_ _25_ _47_ vdd gnd INVX1
X_267_ _181_[0] _76_ vdd gnd INVX1
X_399_ vdd gnd _181_[3] rx_data[3] BUFX2
X_420_ gnd vdd _6_[3] vdd _10__bF$buf3 tx_cnt[3] 
+ txclk
+ DFFSR
X_323_ tx_data[0] _120_ vdd gnd INVX1
X_361_ vdd gnd _145_ _148_ _149_ AND2X2
X_417_ gnd vdd _6_[0] vdd _10__bF$buf0 tx_cnt[0] 
+ txclk
+ DFFSR
X_226_ gnd vdd rx_d2 _43_ _44_ _11_ OAI21X1
X_264_ _26_ vdd gnd rx_cnt[3] _37_ _74_ NAND3X1
X_358_ _143_ vdd gnd _123_ _142_ _146_ NAND3X1
X_396_ vdd gnd _181_[0] rx_data[0] BUFX2
X_299_ vdd _93_ gnd _99_ tx_cnt[1] NOR2X1
XFILL41640x12150 vdd gnd FILL
X_320_ gnd vdd _100_ _114_ _118_ _117_ AOI21X1
X_414_ gnd vdd _9_[5] vdd _10__bF$buf0 tx_reg[5] 
+ txclk
+ DFFSR
X_223_ gnd vdd _177_ _40_ _4_[0] _41_ OAI21X1
X_261_ rx_sample_cnt[3] _72_ vdd gnd INVX1
X_317_ _97_ _115_ vdd gnd INVX1
X_355_ gnd vdd tx_cnt[0] tx_cnt[1] _143_ tx_cnt[2] OAI21X1
X_393_ vdd _176_ gnd _74_ _49_ NAND2X1
XFILL41640x9150 vdd gnd FILL
X_258_ gnd vdd rx_busy _179_ _70_ _62_ AOI21X1
X_296_ gnd vdd _95_ _96_ _6_[0] _92_ AOI21X1
X_199_ vdd _21_ gnd _14_ _20_ NAND2X1
X_411_ gnd vdd _9_[2] vdd _10__bF$buf0 tx_reg[2] 
+ txclk
+ DFFSR
X_220_ _38_ _39_ vdd gnd INVX1
X_314_ _112_ _6_[2] vdd gnd INVX1
X_352_ _93_ vdd gnd tx_cnt[2] _102_ _140_ NAND3X1
X_408_ gnd vdd _7_ _10__bF$buf3 vdd _183_ 
+ txclk
+ DFFSR
X_217_ gnd vdd _177_ _35_ _4_[1] _36_ OAI21X1
X_390_ vdd _174_ gnd _39_ _167_ NAND2X1
X_446_ gnd vdd rx_in _10__bF$buf4 vdd rx_d1 
+ rxclk_bF$buf3
+ DFFSR
XFILL41880x21150 vdd gnd FILL
X_255_ gnd vdd rx_sample_cnt[1] _63_ _68_ _67_ OAI21X1
X_293_ _183_ _94_ vdd gnd INVX1
X_349_ vdd _102_ gnd _137_ tx_cnt[0] NOR2X1
X_387_ vdd _172_ gnd _34_ _167_ NAND2X1
X_196_ gnd vdd _17_ rx_cnt[2] _18_ rx_cnt[3] AOI21X1
X_311_ gnd vdd _183_ _109_ _110_ _98_ OAI21X1
X_405_ vdd gnd _183_ tx_empty BUFX2
X_214_ _33_ _34_ vdd gnd INVX1
X_443_ gnd vdd _1_[1] vdd _10__bF$buf2 rx_cnt[1] 
+ rxclk_bF$buf1
+ DFFSR
X_252_ vdd _65_ gnd rx_sample_cnt[1] rx_sample_cnt[0] NAND2X1
X_308_ vdd _101_ gnd _6_[1] _107_ NOR2X1
X_290_ gnd vdd uld_rx_data_bF$buf0 _90_ _2_[7] _91_ OAI21X1
X_346_ tx_data[7] _136_ vdd gnd INVX1
XFILL41640x150 vdd gnd FILL
X_384_ vdd gnd _59_ _53_ _170_ AND2X2
XFILL41880x15150 vdd gnd FILL
X_193_ rx_cnt[1] _15_ vdd gnd INVX1
X_249_ vdd _43_ gnd _63_ _61_ NOR2X1
X_287_ gnd vdd uld_rx_data_bF$buf3 _88_ _2_[6] _89_ OAI21X1
X_402_ vdd gnd _181_[6] rx_data[6] BUFX2
X_211_ gnd vdd _177_ _30_ _4_[2] _31_ OAI21X1
X_440_ gnd vdd _5_[2] vdd _10__bF$buf2 rx_sample_cnt[2] 
+ rxclk_bF$buf1
+ DFFSR
X_305_ vdd _105_ gnd _103_ _104_ NAND2X1
X_343_ vdd _121_ gnd _134_ tx_reg[6] NOR2X1
X_208_ _28_ vdd gnd _27_ _18_ _29_ NAND3X1
X_381_ vdd _168_ gnd _14_ _167_ NAND2X1
X_437_ gnd vdd _4_[7] vdd _10__bF$buf5 rx_reg[7] 
+ rxclk_bF$buf0
+ DFFSR
X_190_ _11_ _12_ vdd gnd INVX1
X_246_ gnd vdd _22_ _58_ _1_[3] _60_ OAI21X1
X_284_ gnd vdd uld_rx_data_bF$buf2 _86_ _2_[5] _87_ OAI21X1
XFILL41640x6150 vdd gnd FILL
X_378_ gnd vdd rx_cnt[2] _17_ _165_ _23_ OAI21X1
X_187_ rx_sample_cnt[0] vdd gnd rx_sample_cnt[1] rx_sample_cnt[2] _179_ NAND3X1
XFILL41880x9150 vdd gnd FILL
X_302_ vdd gnd _102_ tx_cnt[1] INVX2
X_340_ tx_data[5] _132_ vdd gnd INVX1
X_205_ rx_cnt[2] _26_ vdd gnd INVX1
X_434_ gnd vdd _4_[4] vdd _10__bF$buf5 rx_reg[4] 
+ rxclk_bF$buf0
+ DFFSR
X_243_ gnd vdd _53_ _54_ _58_ _45_ AOI21X1
X_281_ gnd vdd uld_rx_data_bF$buf0 _84_ _2_[4] _85_ OAI21X1
X_337_ tx_data[4] _130_ vdd gnd INVX1
X_375_ _163_ _97_ vdd gnd _160_ OR2X2
X_278_ gnd vdd uld_rx_data_bF$buf1 _82_ _2_[3] _83_ OAI21X1
X_202_ gnd vdd _23_ _13_ _24_ rx_reg[3] OAI21X1
X_431_ gnd vdd _4_[1] vdd _10__bF$buf1 rx_reg[1] 
+ rxclk_bF$buf2
+ DFFSR
X_240_ vdd _56_ gnd _26_ _55_ NAND2X1
X_334_ tx_data[3] _128_ vdd gnd INVX1
X_372_ gnd vdd _157_ _98_ _160_ _113_ AOI21X1
X_428_ gnd vdd _2_[7] vdd _10__bF$buf0 _181_[7] 
+ rxclk_bF$buf0
+ DFFSR
X_237_ vdd _52_ gnd _53_ _42_ NOR2X1
X_275_ gnd vdd uld_rx_data_bF$buf3 _80_ _2_[2] _81_ OAI21X1
XFILL41880x12150 vdd gnd FILL
X_369_ vdd tx_cnt[1] gnd _157_ tx_cnt[0] NOR2X1
XCLKBUF1_insert10 rxclk vdd gnd rxclk_bF$buf4 CLKBUF1
XCLKBUF1_insert11 rxclk vdd gnd rxclk_bF$buf3 CLKBUF1
XCLKBUF1_insert12 rxclk vdd gnd rxclk_bF$buf2 CLKBUF1
XCLKBUF1_insert13 rxclk vdd gnd rxclk_bF$buf1 CLKBUF1
XCLKBUF1_insert14 rxclk vdd gnd rxclk_bF$buf0 CLKBUF1
X_331_ vdd _121_ gnd _126_ tx_reg[2] NOR2X1
X_425_ gnd vdd _2_[4] vdd _10__bF$buf5 _181_[4] 
+ rxclk_bF$buf0
+ DFFSR
X_234_ gnd vdd _32_ _37_ _51_ _47_ OAI21X1
X_272_ gnd vdd uld_rx_data_bF$buf2 _78_ _2_[1] _79_ OAI21X1
X_328_ tx_data[1] _124_ vdd gnd INVX1
X_366_ vdd _154_ gnd tx_cnt[2] _129_ NAND2X1
X_269_ gnd vdd uld_rx_data_bF$buf1 _76_ _2_[0] _77_ OAI21X1
XFILL41640x3150 vdd gnd FILL
XFILL41880x6150 vdd gnd FILL
X_422_ gnd vdd _2_[1] vdd _10__bF$buf1 _181_[1] 
+ rxclk_bF$buf2
+ DFFSR
X_231_ rx_d2 vdd gnd _22_ _48_ _49_ NAND3X1
X_325_ vdd _121_ gnd _122_ tx_reg[0] NOR2X1
X_363_ _139_ vdd gnd tx_reg[6] _140_ _151_ NAND3X1
X_419_ gnd vdd _6_[2] vdd _10__bF$buf0 tx_cnt[2] 
+ txclk
+ DFFSR
X_228_ _45_ _46_ vdd gnd INVX1
X_266_ gnd vdd _75_ _47_ _3_ _73_ AOI21X1
X_398_ vdd gnd _181_[2] rx_data[2] BUFX2
X_322_ vdd _118_ gnd _6_[3] _119_ NOR2X1
X_360_ gnd vdd _146_ _147_ _148_ tx_cnt[0] AOI21X1
X_416_ gnd vdd _9_[7] vdd _10__bF$buf3 tx_reg[7] 
+ txclk
+ DFFSR
X_225_ rx_enable _43_ vdd gnd INVX1
X_263_ vdd uld_rx_data_bF$buf3 gnd _73_ _182_ NOR2X1
X_319_ gnd vdd tx_cnt[3] _116_ _117_ _115_ OAI21X1
X_357_ _144_ vdd gnd _141_ _138_ _145_ NAND3X1
X_395_ vdd gnd reset _10_ INVX8
X_298_ tx_cnt[2] _98_ vdd gnd INVX1
X_413_ gnd vdd _9_[4] vdd _10__bF$buf3 tx_reg[4] 
+ txclk
+ DFFSR
X_222_ gnd vdd _38_ _19_ _41_ rx_reg[0] OAI21X1
X_260_ gnd vdd _69_ _71_ _5_[2] _70_ AOI21X1
X_316_ gnd vdd _108_ tx_cnt[2] _114_ _113_ AOI21X1
X_354_ _98_ vdd gnd _93_ _102_ _142_ NAND3X1
X_219_ _37_ vdd gnd _12_ _180_ _38_ NAND3X1
X_392_ gnd vdd _177_ _174_ _4_[4] _175_ OAI21X1
X_448_ gnd vdd _0_ vdd _10__bF$buf2 rx_busy 
+ rxclk_bF$buf1
+ DFFSR
X_257_ rx_sample_cnt[2] _69_ vdd gnd INVX1
X_295_ vdd _96_ gnd tx_cnt[0] _183_ NAND2X1
X_389_ gnd vdd _177_ _172_ _4_[5] _173_ OAI21X1
X_198_ _19_ _20_ vdd gnd INVX1
X_410_ gnd vdd _9_[1] vdd _10__bF$buf0 tx_reg[1] 
+ txclk
+ DFFSR
XFILL41880x3150 vdd gnd FILL
X_313_ _111_ vdd gnd tx_enable _110_ _112_ NAND3X1
X_351_ gnd vdd tx_cnt[0] tx_cnt[1] _139_ _98_ OAI21X1
X_407_ gnd vdd _8_ _10__bF$buf3 vdd _184_ 
+ txclk
+ DFFSR
X_216_ gnd vdd _33_ _19_ _36_ rx_reg[1] OAI21X1
X_445_ gnd vdd _1_[3] vdd _10__bF$buf2 rx_cnt[3] 
+ rxclk_bF$buf1
+ DFFSR
X_254_ vdd _66_ gnd _67_ _42_ NOR2X1
X_292_ vdd gnd _93_ tx_cnt[0] INVX2
X_348_ gnd vdd _100_ _97_ _94_ ld_tx_data 
+ _7_
+ OAI22X1
X_386_ gnd vdd _170_ _177_ _4_[6] _171_ AOI21X1
X_195_ vdd _17_ gnd _15_ _16_ NAND2X1
X_289_ vdd _91_ gnd uld_rx_data_bF$buf0 rx_reg[7] NAND2X1
X_310_ _108_ _109_ vdd gnd INVX1
X_404_ vdd gnd _182_ rx_empty BUFX2
X_213_ _32_ vdd gnd _12_ _180_ _33_ NAND3X1
X_442_ gnd vdd _1_[0] vdd _10__bF$buf2 rx_cnt[0] 
+ rxclk_bF$buf4
+ DFFSR
X_251_ rx_sample_cnt[1] _64_ vdd gnd INVX1
X_307_ gnd vdd _97_ _105_ _107_ _106_ OAI21X1
X_345_ tx_reg[7] _135_ vdd gnd INVX1
X_383_ gnd vdd _177_ _168_ _4_[7] _169_ OAI21X1
X_439_ gnd vdd _5_[1] vdd _10__bF$buf4 rx_sample_cnt[1] 
+ rxclk_bF$buf3
+ DFFSR
X_192_ _13_ _14_ vdd gnd INVX1
X_248_ _44_ _62_ vdd gnd INVX1
X_286_ vdd _89_ gnd uld_rx_data_bF$buf3 rx_reg[6] NAND2X1
X_189_ vdd _11_ gnd rx_busy rx_enable NAND2X1
X_401_ vdd gnd _181_[5] rx_data[5] BUFX2
X_210_ gnd vdd _25_ _29_ _31_ rx_reg[2] OAI21X1
X_304_ vdd _104_ gnd tx_cnt[1] _93_ NAND2X1
X_342_ tx_data[6] _133_ vdd gnd INVX1
X_207_ vdd _16_ gnd _28_ _15_ NOR2X1
X_380_ _166_ _167_ vdd gnd INVX1
X_436_ gnd vdd _4_[6] vdd _10__bF$buf4 rx_reg[6] 
+ rxclk_bF$buf3
+ DFFSR
X_245_ vdd _60_ gnd _53_ _59_ NAND2X1
X_283_ vdd _87_ gnd uld_rx_data_bF$buf2 rx_reg[5] NAND2X1
X_339_ tx_reg[5] _131_ vdd gnd INVX1
X_377_ gnd vdd _149_ _162_ _8_ _164_ OAI21X1
X_186_ vdd rx_cnt[0] gnd _178_ rx_cnt[1] NOR2X1
X_301_ vdd _100_ gnd _101_ _97_ NOR2X1
X_204_ vdd _25_ gnd _12_ _180_ NAND2X1
X_433_ gnd vdd _4_[3] vdd _10__bF$buf5 rx_reg[3] 
+ rxclk_bF$buf4
+ DFFSR
X_242_ gnd vdd _26_ _46_ _1_[2] _57_ OAI21X1
X_280_ vdd _85_ gnd uld_rx_data_bF$buf0 rx_reg[4] NAND2X1
X_336_ tx_reg[4] _129_ vdd gnd INVX1
X_374_ gnd vdd _152_ _156_ _162_ _161_ OAI21X1
X_239_ vdd _55_ gnd rx_enable _28_ NAND2X1
X_277_ vdd _83_ gnd rx_reg[3] uld_rx_data_bF$buf1 NAND2X1
X_201_ vdd _23_ gnd rx_cnt[2] _22_ NAND2X1
X_430_ gnd vdd _4_[0] vdd _10__bF$buf5 rx_reg[0] 
+ rxclk_bF$buf4
+ DFFSR
X_333_ tx_reg[3] _127_ vdd gnd INVX1
X_371_ vdd gnd _157_ _158_ _159_ AND2X2
X_427_ gnd vdd _2_[6] vdd _10__bF$buf1 _181_[6] 
+ rxclk_bF$buf4
+ DFFSR
X_236_ _52_ rx_sample_cnt[3] vdd gnd _179_ OR2X2
X_274_ vdd _81_ gnd rx_reg[2] uld_rx_data_bF$buf3 NAND2X1
X_368_ gnd vdd _153_ _155_ _156_ tx_cnt[0] OAI21X1
X_330_ tx_data[2] _125_ vdd gnd INVX1
X_424_ gnd vdd _2_[3] vdd _10__bF$buf2 _181_[3] 
+ rxclk_bF$buf1
+ DFFSR
X_233_ gnd vdd _16_ _46_ _1_[0] _50_ OAI21X1
X_271_ vdd _79_ gnd rx_reg[1] uld_rx_data_bF$buf2 NAND2X1
X_327_ tx_reg[1] _123_ vdd gnd INVX1
X_365_ gnd vdd _140_ _139_ _153_ tx_reg[0] AOI21X1
X_268_ vdd _77_ gnd rx_reg[0] uld_rx_data_bF$buf1 NAND2X1
X_421_ gnd vdd _2_[0] vdd _10__bF$buf5 _181_[0] 
+ rxclk_bF$buf0
+ DFFSR
X_230_ _27_ _48_ vdd gnd INVX1
X_324_ vdd gnd _183_ ld_tx_data _121_ AND2X2
X_362_ _143_ vdd gnd tx_reg[2] _142_ _150_ NAND3X1
X_418_ gnd vdd _6_[1] vdd _10__bF$buf2 tx_cnt[1] 
+ txclk
+ DFFSR
X_227_ gnd vdd _42_ _180_ _45_ _44_ OAI21X1
X_265_ vdd _74_ gnd _75_ _177_ NOR2X1
X_359_ gnd vdd _103_ _104_ _147_ tx_cnt[2] 
+ _131_
+ AOI22X1

.ends
.end
