/* Verilog module written by vlog2Verilog (qflow) */
/* With explicit power connections */

module uart(
    inout vdd,
    inout gnd,
    input ld_tx_data,
    input reset,
    output [7:0] rx_data,
    output rx_empty,
    input rx_enable,
    input rx_in,
    input rxclk,
    input [7:0] tx_data,
    output tx_empty,
    input tx_enable,
    output tx_out,
    input txclk,
    input uld_rx_data
);

wire _168_ ;
wire _60_ ;
wire _19_ ;
wire _57_ ;
wire _130_ ;
wire _95_ ;
wire _127_ ;
wire _165_ ;
wire _16_ ;
wire _54_ ;
wire _92_ ;
wire _124_ ;
wire tx_out ;
wire _89_ ;
wire _162_ ;
wire rx_empty ;
wire _13_ ;
wire _159_ ;
wire _51_ ;
wire _7_ ;
wire _48_ ;
wire _121_ ;
wire _86_ ;
wire _118_ ;
wire _10_ ;
wire _156_ ;
wire ld_tx_data ;
wire [7:0] _4_ ;
wire _45_ ;
wire rxclk_bF$buf0 ;
wire rxclk_bF$buf1 ;
wire rxclk_bF$buf2 ;
wire rxclk_bF$buf3 ;
wire rxclk_bF$buf4 ;
wire _83_ ;
wire _115_ ;
wire _153_ ;
wire txclk ;
wire [3:0] _1_ ;
wire _42_ ;
wire _80_ ;
wire _39_ ;
wire _112_ ;
wire _77_ ;
wire _150_ ;
wire _109_ ;
wire _147_ ;
wire _36_ ;
wire _74_ ;
wire _106_ ;
wire _144_ ;
wire _182_ ;
wire _33_ ;
wire _179_ ;
wire _71_ ;
wire _103_ ;
wire _68_ ;
wire _141_ ;
wire _138_ ;
wire _30_ ;
wire rx_in ;
wire _176_ ;
wire _27_ ;
wire _100_ ;
wire _65_ ;
wire [7:0] rx_reg ;
wire _135_ ;
wire _173_ ;
wire [7:0] tx_reg ;
wire _24_ ;
wire _62_ ;
wire [7:0] tx_data ;
wire _59_ ;
wire _132_ ;
wire _97_ ;
wire _170_ ;
wire _129_ ;
wire _21_ ;
wire _167_ ;
wire _18_ ;
wire _56_ ;
wire _94_ ;
wire _126_ ;
wire _164_ ;
wire _15_ ;
wire _53_ ;
wire _91_ ;
wire [7:0] _9_ ;
wire _123_ ;
wire _88_ ;
wire _161_ ;
wire _12_ ;
wire _158_ ;
wire _50_ ;
wire [3:0] _6_ ;
wire _47_ ;
wire _120_ ;
wire _85_ ;
wire _117_ ;
wire _155_ ;
wire _3_ ;
wire _44_ ;
wire _82_ ;
wire uld_rx_data ;
wire _114_ ;
wire _79_ ;
wire _152_ ;
wire _0_ ;
wire _149_ ;
wire _41_ ;
wire [3:0] rx_cnt ;
wire _38_ ;
wire _111_ ;
wire _76_ ;
wire [3:0] tx_cnt ;
wire _108_ ;
wire _146_ ;
wire _184_ ;
wire _35_ ;
wire _73_ ;
wire rxclk ;
wire _10__bF$buf0 ;
wire _10__bF$buf1 ;
wire _10__bF$buf2 ;
wire _10__bF$buf3 ;
wire _10__bF$buf4 ;
wire _10__bF$buf5 ;
wire _105_ ;
wire _143_ ;
wire [7:0] _181_ ;
wire _32_ ;
wire rx_enable ;
wire _178_ ;
wire _70_ ;
wire _29_ ;
wire _102_ ;
wire _67_ ;
wire _140_ ;
wire _137_ ;
wire _175_ ;
wire uld_rx_data_bF$buf0 ;
wire uld_rx_data_bF$buf1 ;
wire uld_rx_data_bF$buf2 ;
wire uld_rx_data_bF$buf3 ;
wire _26_ ;
wire _64_ ;
wire _134_ ;
wire _99_ ;
wire _172_ ;
wire _23_ ;
wire _169_ ;
wire _61_ ;
wire _58_ ;
wire _131_ ;
wire _96_ ;
wire _128_ ;
wire _20_ ;
wire _166_ ;
wire [7:0] rx_data ;
wire rx_busy ;
wire [3:0] rx_sample_cnt ;
wire _17_ ;
wire _55_ ;
wire _93_ ;
wire _125_ ;
wire _163_ ;
wire _14_ ;
wire _52_ ;
wire _90_ ;
wire _8_ ;
wire _49_ ;
wire _122_ ;
wire _87_ ;
wire _160_ ;
wire tx_enable ;
wire _119_ ;
wire _11_ ;
wire _157_ ;
wire [3:0] _5_ ;
wire _46_ ;
wire _84_ ;
wire _116_ ;
wire _154_ ;
wire [7:0] _2_ ;
wire _43_ ;
wire _81_ ;
wire _113_ ;
wire _78_ ;
wire _151_ ;
wire _148_ ;
wire _40_ ;
wire _37_ ;
wire _110_ ;
wire _75_ ;
wire _107_ ;
wire _145_ ;
wire _183_ ;
wire _34_ ;
wire _72_ ;
wire _104_ ;
wire _69_ ;
wire _142_ ;
wire _180_ ;
wire _139_ ;
wire _31_ ;
wire _177_ ;
wire _28_ ;
wire _101_ ;
wire tx_empty ;
wire _66_ ;
wire _136_ ;
wire _174_ ;
wire _25_ ;
wire _63_ ;
wire reset ;
wire _133_ ;
wire rx_d1 ;
wire rx_d2 ;
wire _98_ ;
wire _171_ ;
wire _22_ ;

BUFX2 _397_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[1]),
    .Y(rx_data[1])
);

OAI21X1 _321_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[3]),
    .B(_94_),
    .C(tx_enable),
    .Y(_119_)
);

DFFSR _415_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[6]),
    .CLK(txclk),
    .R(_10__bF$buf3),
    .S(vdd),
    .D(_9_[6])
);

INVX1 _224_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_busy),
    .Y(_42_)
);

OAI21X1 _262_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_72_),
    .B(_70_),
    .C(_25_),
    .Y(_5_[3])
);

NAND2X1 _318_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[2]),
    .B(_108_),
    .Y(_116_)
);

NAND3X1 _356_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_127_),
    .B(_143_),
    .C(_142_),
    .Y(_144_)
);

AOI21X1 _394_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_176_),
    .B(_53_),
    .C(_62_),
    .Y(_0_)
);

NAND3X1 _259_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_busy),
    .B(rx_enable),
    .C(_66_),
    .Y(_71_)
);

NAND2X1 _297_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_enable),
    .B(_94_),
    .Y(_97_)
);

DFFSR _412_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[3]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_9_[3])
);

NAND2X1 _221_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_39_),
    .B(_20_),
    .Y(_40_)
);

INVX1 _315_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[3]),
    .Y(_113_)
);

NAND3X1 _353_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_135_),
    .B(_139_),
    .C(_140_),
    .Y(_141_)
);

DFFSR _409_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[0]),
    .CLK(txclk),
    .R(_10__bF$buf3),
    .S(vdd),
    .D(_9_[0])
);

NOR2X1 _218_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[1]),
    .B(_16_),
    .Y(_37_)
);

OAI21X1 _391_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_38_),
    .B(_166_),
    .C(rx_reg[4]),
    .Y(_175_)
);

DFFSR _447_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_d2),
    .CLK(rxclk_bF$buf3),
    .R(vdd),
    .S(_10__bF$buf4),
    .D(rx_d1)
);

OAI21X1 _256_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_64_),
    .B(_44_),
    .C(_68_),
    .Y(_5_[1])
);

NAND2X1 _294_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_93_),
    .B(_94_),
    .Y(_95_)
);

FILL FILL41640x24150 (
    .gnd(gnd),
    .vdd(vdd)
);

OAI21X1 _388_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_33_),
    .B(_166_),
    .C(rx_reg[5]),
    .Y(_173_)
);

OAI21X1 _197_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[2]),
    .B(_17_),
    .C(_18_),
    .Y(_19_)
);

NAND3X1 _312_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_94_),
    .B(tx_cnt[2]),
    .C(_108_),
    .Y(_111_)
);

NOR2X1 _350_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_99_),
    .B(_137_),
    .Y(_138_)
);

BUFX2 _406_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_184_),
    .Y(tx_out)
);

NAND2X1 _215_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_34_),
    .B(_20_),
    .Y(_35_)
);

DFFSR _444_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_cnt[2]),
    .CLK(rxclk_bF$buf4),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_1_[2])
);

INVX1 _253_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_65_),
    .Y(_66_)
);

NOR2X1 _309_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_93_),
    .B(_102_),
    .Y(_108_)
);

FILL FILL41880x150 (
    .gnd(gnd),
    .vdd(vdd)
);

INVX1 _291_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_enable),
    .Y(_92_)
);

MUX2X1 _347_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_136_),
    .B(_135_),
    .S(_121_),
    .Y(_9_[7])
);

AOI21X1 _385_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_59_),
    .B(_53_),
    .C(rx_reg[6]),
    .Y(_171_)
);

INVX1 _194_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[0]),
    .Y(_16_)
);

INVX1 _288_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[7]),
    .Y(_90_)
);

BUFX2 _403_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[7]),
    .Y(rx_data[7])
);

NOR2X1 _212_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[0]),
    .B(_15_),
    .Y(_32_)
);

DFFSR _441_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_sample_cnt[3]),
    .CLK(rxclk_bF$buf3),
    .R(_10__bF$buf4),
    .S(vdd),
    .D(_5_[3])
);

AOI22X1 _250_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_busy),
    .B(_63_),
    .C(_62_),
    .D(_61_),
    .Y(_5_[0])
);

AOI21X1 _306_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_102_),
    .B(_183_),
    .C(_92_),
    .Y(_106_)
);

AOI21X1 _344_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_133_),
    .B(_121_),
    .C(_134_),
    .Y(_9_[6])
);

OR2X2 _209_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_29_),
    .B(_25_),
    .Y(_30_)
);

OAI21X1 _382_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_13_),
    .B(_166_),
    .C(rx_reg[7]),
    .Y(_169_)
);

DFFSR _438_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_sample_cnt[0]),
    .CLK(rxclk_bF$buf3),
    .R(_10__bF$buf4),
    .S(vdd),
    .D(_5_[0])
);

NAND3X1 _191_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_178_),
    .B(_12_),
    .C(_180_),
    .Y(_13_)
);

INVX1 _247_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[0]),
    .Y(_61_)
);

INVX1 _285_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[6]),
    .Y(_88_)
);

OAI21X1 _379_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[3]),
    .B(_17_),
    .C(_165_),
    .Y(_166_)
);

NOR2X1 _188_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[3]),
    .B(_179_),
    .Y(_180_)
);

BUFX2 _400_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[4]),
    .Y(rx_data[4])
);

NAND2X1 _303_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(_102_),
    .Y(_103_)
);

MUX2X1 _341_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_132_),
    .B(_131_),
    .S(_121_),
    .Y(_9_[5])
);

NAND2X1 _206_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_26_),
    .B(_178_),
    .Y(_27_)
);

FILL FILL41880x27150 (
    .gnd(gnd),
    .vdd(vdd)
);

DFFSR _435_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[5]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_4_[5])
);

NOR2X1 _244_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_23_),
    .B(_55_),
    .Y(_59_)
);

INVX1 _282_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[5]),
    .Y(_86_)
);

MUX2X1 _338_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_130_),
    .B(_129_),
    .S(_121_),
    .Y(_9_[4])
);

AOI21X1 _376_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_184_),
    .B(_163_),
    .C(_101_),
    .Y(_164_)
);

INVX2 _185_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_d2),
    .Y(_177_)
);

FILL FILL41640x21150 (
    .gnd(gnd),
    .vdd(vdd)
);

INVX1 _279_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[4]),
    .Y(_84_)
);

NAND3X1 _300_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[3]),
    .B(_98_),
    .C(_99_),
    .Y(_100_)
);

OAI21X1 _203_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_21_),
    .C(_24_),
    .Y(_4_[3])
);

DFFSR _432_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[2]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_4_[2])
);

NAND3X1 _241_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_54_),
    .B(_56_),
    .C(_53_),
    .Y(_57_)
);

MUX2X1 _335_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_128_),
    .B(_127_),
    .S(_121_),
    .Y(_9_[3])
);

NOR3X1 _373_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_97_),
    .B(_160_),
    .C(_159_),
    .Y(_161_)
);

DFFSR _429_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_182_),
    .CLK(rxclk_bF$buf4),
    .R(vdd),
    .S(_10__bF$buf4),
    .D(_3_)
);

NAND2X1 _238_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[2]),
    .B(_28_),
    .Y(_54_)
);

INVX1 _276_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[3]),
    .Y(_82_)
);

INVX1 _200_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[3]),
    .Y(_22_)
);

AOI21X1 _332_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_125_),
    .B(_121_),
    .C(_126_),
    .Y(_9_[2])
);

NOR2X1 _370_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[3]),
    .B(tx_cnt[2]),
    .Y(_158_)
);

DFFSR _426_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[5]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_2_[5])
);

OAI21X1 _235_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_15_),
    .B(_46_),
    .C(_51_),
    .Y(_1_[1])
);

INVX1 _273_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[2]),
    .Y(_80_)
);

MUX2X1 _329_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_124_),
    .B(_123_),
    .S(_121_),
    .Y(_9_[1])
);

OAI21X1 _367_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_99_),
    .B(_137_),
    .C(_154_),
    .Y(_155_)
);

BUFX2 BUFX2_insert0 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf5)
);

BUFX2 BUFX2_insert1 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf4)
);

BUFX2 BUFX2_insert2 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf3)
);

BUFX2 BUFX2_insert3 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf2)
);

BUFX2 BUFX2_insert4 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf1)
);

BUFX2 BUFX2_insert5 (
    .gnd(gnd),
    .vdd(vdd),
    .A(_10_),
    .Y(_10__bF$buf0)
);

BUFX2 BUFX2_insert6 (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data),
    .Y(uld_rx_data_bF$buf3)
);

BUFX2 BUFX2_insert7 (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data),
    .Y(uld_rx_data_bF$buf2)
);

BUFX2 BUFX2_insert8 (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data),
    .Y(uld_rx_data_bF$buf1)
);

BUFX2 BUFX2_insert9 (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data),
    .Y(uld_rx_data_bF$buf0)
);

DFFSR _423_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[2]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_2_[2])
);

NAND3X1 _232_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_16_),
    .B(_47_),
    .C(_49_),
    .Y(_50_)
);

INVX1 _270_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[1]),
    .Y(_78_)
);

AOI21X1 _326_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_120_),
    .B(_121_),
    .C(_122_),
    .Y(_9_[0])
);

FILL FILL41880x24150 (
    .gnd(gnd),
    .vdd(vdd)
);

AOI21X1 _364_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_150_),
    .B(_151_),
    .C(_105_),
    .Y(_152_)
);

INVX1 _229_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_25_),
    .Y(_47_)
);

INVX1 _267_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[0]),
    .Y(_76_)
);

BUFX2 _399_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[3]),
    .Y(rx_data[3])
);

DFFSR _420_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_cnt[3]),
    .CLK(txclk),
    .R(_10__bF$buf3),
    .S(vdd),
    .D(_6_[3])
);

INVX1 _323_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[0]),
    .Y(_120_)
);

AND2X2 _361_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_145_),
    .B(_148_),
    .Y(_149_)
);

DFFSR _417_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_cnt[0]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_6_[0])
);

OAI21X1 _226_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_d2),
    .B(_43_),
    .C(_11_),
    .Y(_44_)
);

NAND3X1 _264_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[3]),
    .B(_26_),
    .C(_37_),
    .Y(_74_)
);

NAND3X1 _358_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_123_),
    .B(_143_),
    .C(_142_),
    .Y(_146_)
);

BUFX2 _396_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[0]),
    .Y(rx_data[0])
);

NOR2X1 _299_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[1]),
    .B(_93_),
    .Y(_99_)
);

FILL FILL41640x12150 (
    .gnd(gnd),
    .vdd(vdd)
);

AOI21X1 _320_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_100_),
    .B(_114_),
    .C(_117_),
    .Y(_118_)
);

DFFSR _414_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[5]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_9_[5])
);

OAI21X1 _223_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_40_),
    .C(_41_),
    .Y(_4_[0])
);

INVX1 _261_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[3]),
    .Y(_72_)
);

INVX1 _317_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_97_),
    .Y(_115_)
);

OAI21X1 _355_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(tx_cnt[1]),
    .C(tx_cnt[2]),
    .Y(_143_)
);

NAND2X1 _393_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_74_),
    .B(_49_),
    .Y(_176_)
);

FILL FILL41640x9150 (
    .gnd(gnd),
    .vdd(vdd)
);

AOI21X1 _258_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_busy),
    .B(_179_),
    .C(_62_),
    .Y(_70_)
);

AOI21X1 _296_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_95_),
    .B(_96_),
    .C(_92_),
    .Y(_6_[0])
);

NAND2X1 _199_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_14_),
    .B(_20_),
    .Y(_21_)
);

DFFSR _411_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[2]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_9_[2])
);

INVX1 _220_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_38_),
    .Y(_39_)
);

INVX1 _314_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_112_),
    .Y(_6_[2])
);

NAND3X1 _352_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[2]),
    .B(_93_),
    .C(_102_),
    .Y(_140_)
);

DFFSR _408_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_183_),
    .CLK(txclk),
    .R(vdd),
    .S(_10__bF$buf3),
    .D(_7_)
);

OAI21X1 _217_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_35_),
    .C(_36_),
    .Y(_4_[1])
);

NAND2X1 _390_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_39_),
    .B(_167_),
    .Y(_174_)
);

DFFSR _446_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_d1),
    .CLK(rxclk_bF$buf3),
    .R(vdd),
    .S(_10__bF$buf4),
    .D(rx_in)
);

FILL FILL41880x21150 (
    .gnd(gnd),
    .vdd(vdd)
);

OAI21X1 _255_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[1]),
    .B(_63_),
    .C(_67_),
    .Y(_68_)
);

INVX1 _293_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_183_),
    .Y(_94_)
);

NOR2X1 _349_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(_102_),
    .Y(_137_)
);

NAND2X1 _387_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_34_),
    .B(_167_),
    .Y(_172_)
);

AOI21X1 _196_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_17_),
    .B(rx_cnt[2]),
    .C(rx_cnt[3]),
    .Y(_18_)
);

OAI21X1 _311_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_183_),
    .B(_109_),
    .C(_98_),
    .Y(_110_)
);

BUFX2 _405_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_183_),
    .Y(tx_empty)
);

INVX1 _214_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_33_),
    .Y(_34_)
);

DFFSR _443_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_cnt[1]),
    .CLK(rxclk_bF$buf1),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_1_[1])
);

NAND2X1 _252_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[1]),
    .B(rx_sample_cnt[0]),
    .Y(_65_)
);

NOR2X1 _308_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_107_),
    .B(_101_),
    .Y(_6_[1])
);

OAI21X1 _290_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf0),
    .B(_90_),
    .C(_91_),
    .Y(_2_[7])
);

INVX1 _346_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[7]),
    .Y(_136_)
);

FILL FILL41640x150 (
    .gnd(gnd),
    .vdd(vdd)
);

AND2X2 _384_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_59_),
    .B(_53_),
    .Y(_170_)
);

FILL FILL41880x15150 (
    .gnd(gnd),
    .vdd(vdd)
);

INVX1 _193_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[1]),
    .Y(_15_)
);

NOR2X1 _249_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_61_),
    .B(_43_),
    .Y(_63_)
);

OAI21X1 _287_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf3),
    .B(_88_),
    .C(_89_),
    .Y(_2_[6])
);

BUFX2 _402_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[6]),
    .Y(rx_data[6])
);

OAI21X1 _211_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_30_),
    .C(_31_),
    .Y(_4_[2])
);

DFFSR _440_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_sample_cnt[2]),
    .CLK(rxclk_bF$buf1),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_5_[2])
);

NAND2X1 _305_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_103_),
    .B(_104_),
    .Y(_105_)
);

NOR2X1 _343_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[6]),
    .B(_121_),
    .Y(_134_)
);

NAND3X1 _208_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_27_),
    .B(_28_),
    .C(_18_),
    .Y(_29_)
);

NAND2X1 _381_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_14_),
    .B(_167_),
    .Y(_168_)
);

DFFSR _437_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[7]),
    .CLK(rxclk_bF$buf0),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_4_[7])
);

INVX1 _190_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_11_),
    .Y(_12_)
);

OAI21X1 _246_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_22_),
    .B(_58_),
    .C(_60_),
    .Y(_1_[3])
);

OAI21X1 _284_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf2),
    .B(_86_),
    .C(_87_),
    .Y(_2_[5])
);

FILL FILL41640x6150 (
    .gnd(gnd),
    .vdd(vdd)
);

OAI21X1 _378_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[2]),
    .B(_17_),
    .C(_23_),
    .Y(_165_)
);

NAND3X1 _187_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[1]),
    .B(rx_sample_cnt[0]),
    .C(rx_sample_cnt[2]),
    .Y(_179_)
);

FILL FILL41880x9150 (
    .gnd(gnd),
    .vdd(vdd)
);

INVX2 _302_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[1]),
    .Y(_102_)
);

INVX1 _340_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[5]),
    .Y(_132_)
);

INVX1 _205_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[2]),
    .Y(_26_)
);

DFFSR _434_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[4]),
    .CLK(rxclk_bF$buf0),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_4_[4])
);

AOI21X1 _243_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_53_),
    .B(_54_),
    .C(_45_),
    .Y(_58_)
);

OAI21X1 _281_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf0),
    .B(_84_),
    .C(_85_),
    .Y(_2_[4])
);

INVX1 _337_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[4]),
    .Y(_130_)
);

OR2X2 _375_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_160_),
    .B(_97_),
    .Y(_163_)
);

OAI21X1 _278_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf1),
    .B(_82_),
    .C(_83_),
    .Y(_2_[3])
);

OAI21X1 _202_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_23_),
    .B(_13_),
    .C(rx_reg[3]),
    .Y(_24_)
);

DFFSR _431_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[1]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_4_[1])
);

NAND2X1 _240_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_26_),
    .B(_55_),
    .Y(_56_)
);

INVX1 _334_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[3]),
    .Y(_128_)
);

AOI21X1 _372_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_157_),
    .B(_98_),
    .C(_113_),
    .Y(_160_)
);

DFFSR _428_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[7]),
    .CLK(rxclk_bF$buf0),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_2_[7])
);

NOR2X1 _237_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_42_),
    .B(_52_),
    .Y(_53_)
);

OAI21X1 _275_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf3),
    .B(_80_),
    .C(_81_),
    .Y(_2_[2])
);

FILL FILL41880x12150 (
    .gnd(gnd),
    .vdd(vdd)
);

NOR2X1 _369_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(tx_cnt[1]),
    .Y(_157_)
);

CLKBUF1 CLKBUF1_insert10 (
    .gnd(gnd),
    .vdd(vdd),
    .A(rxclk),
    .Y(rxclk_bF$buf4)
);

CLKBUF1 CLKBUF1_insert11 (
    .gnd(gnd),
    .vdd(vdd),
    .A(rxclk),
    .Y(rxclk_bF$buf3)
);

CLKBUF1 CLKBUF1_insert12 (
    .gnd(gnd),
    .vdd(vdd),
    .A(rxclk),
    .Y(rxclk_bF$buf2)
);

CLKBUF1 CLKBUF1_insert13 (
    .gnd(gnd),
    .vdd(vdd),
    .A(rxclk),
    .Y(rxclk_bF$buf1)
);

CLKBUF1 CLKBUF1_insert14 (
    .gnd(gnd),
    .vdd(vdd),
    .A(rxclk),
    .Y(rxclk_bF$buf0)
);

NOR2X1 _331_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[2]),
    .B(_121_),
    .Y(_126_)
);

DFFSR _425_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[4]),
    .CLK(rxclk_bF$buf0),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_2_[4])
);

OAI21X1 _234_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_32_),
    .B(_37_),
    .C(_47_),
    .Y(_51_)
);

OAI21X1 _272_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf2),
    .B(_78_),
    .C(_79_),
    .Y(_2_[1])
);

INVX1 _328_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[1]),
    .Y(_124_)
);

NAND2X1 _366_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[2]),
    .B(_129_),
    .Y(_154_)
);

OAI21X1 _269_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf1),
    .B(_76_),
    .C(_77_),
    .Y(_2_[0])
);

FILL FILL41640x3150 (
    .gnd(gnd),
    .vdd(vdd)
);

FILL FILL41880x6150 (
    .gnd(gnd),
    .vdd(vdd)
);

DFFSR _422_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[1]),
    .CLK(rxclk_bF$buf2),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_2_[1])
);

NAND3X1 _231_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_22_),
    .B(rx_d2),
    .C(_48_),
    .Y(_49_)
);

NOR2X1 _325_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[0]),
    .B(_121_),
    .Y(_122_)
);

NAND3X1 _363_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[6]),
    .B(_139_),
    .C(_140_),
    .Y(_151_)
);

DFFSR _419_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_cnt[2]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_6_[2])
);

INVX1 _228_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_45_),
    .Y(_46_)
);

AOI21X1 _266_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_75_),
    .B(_47_),
    .C(_73_),
    .Y(_3_)
);

BUFX2 _398_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[2]),
    .Y(rx_data[2])
);

NOR2X1 _322_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_119_),
    .B(_118_),
    .Y(_6_[3])
);

AOI21X1 _360_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_146_),
    .B(_147_),
    .C(tx_cnt[0]),
    .Y(_148_)
);

DFFSR _416_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[7]),
    .CLK(txclk),
    .R(_10__bF$buf3),
    .S(vdd),
    .D(_9_[7])
);

INVX1 _225_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_enable),
    .Y(_43_)
);

NOR2X1 _263_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_182_),
    .B(uld_rx_data_bF$buf3),
    .Y(_73_)
);

OAI21X1 _319_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[3]),
    .B(_116_),
    .C(_115_),
    .Y(_117_)
);

NAND3X1 _357_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_141_),
    .B(_144_),
    .C(_138_),
    .Y(_145_)
);

INVX8 _395_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(reset),
    .Y(_10_)
);

INVX1 _298_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[2]),
    .Y(_98_)
);

DFFSR _413_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[4]),
    .CLK(txclk),
    .R(_10__bF$buf3),
    .S(vdd),
    .D(_9_[4])
);

OAI21X1 _222_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_38_),
    .B(_19_),
    .C(rx_reg[0]),
    .Y(_41_)
);

AOI21X1 _260_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_69_),
    .B(_71_),
    .C(_70_),
    .Y(_5_[2])
);

AOI21X1 _316_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_108_),
    .B(tx_cnt[2]),
    .C(_113_),
    .Y(_114_)
);

NAND3X1 _354_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_93_),
    .B(_98_),
    .C(_102_),
    .Y(_142_)
);

NAND3X1 _219_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_12_),
    .B(_37_),
    .C(_180_),
    .Y(_38_)
);

OAI21X1 _392_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_174_),
    .C(_175_),
    .Y(_4_[4])
);

DFFSR _448_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_busy),
    .CLK(rxclk_bF$buf1),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_0_)
);

INVX1 _257_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[2]),
    .Y(_69_)
);

NAND2X1 _295_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(_183_),
    .Y(_96_)
);

OAI21X1 _389_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_172_),
    .C(_173_),
    .Y(_4_[5])
);

INVX1 _198_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_19_),
    .Y(_20_)
);

DFFSR _410_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_reg[1]),
    .CLK(txclk),
    .R(_10__bF$buf0),
    .S(vdd),
    .D(_9_[1])
);

FILL FILL41880x3150 (
    .gnd(gnd),
    .vdd(vdd)
);

NAND3X1 _313_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_enable),
    .B(_111_),
    .C(_110_),
    .Y(_112_)
);

OAI21X1 _351_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .B(tx_cnt[1]),
    .C(_98_),
    .Y(_139_)
);

DFFSR _407_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_184_),
    .CLK(txclk),
    .R(vdd),
    .S(_10__bF$buf3),
    .D(_8_)
);

OAI21X1 _216_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_33_),
    .B(_19_),
    .C(rx_reg[1]),
    .Y(_36_)
);

DFFSR _445_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_cnt[3]),
    .CLK(rxclk_bF$buf1),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_1_[3])
);

NOR2X1 _254_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_42_),
    .B(_66_),
    .Y(_67_)
);

INVX2 _292_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[0]),
    .Y(_93_)
);

OAI22X1 _348_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_94_),
    .B(ld_tx_data),
    .C(_97_),
    .D(_100_),
    .Y(_7_)
);

AOI21X1 _386_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_170_),
    .B(_177_),
    .C(_171_),
    .Y(_4_[6])
);

NAND2X1 _195_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_15_),
    .B(_16_),
    .Y(_17_)
);

NAND2X1 _289_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf0),
    .B(rx_reg[7]),
    .Y(_91_)
);

INVX1 _310_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_108_),
    .Y(_109_)
);

BUFX2 _404_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_182_),
    .Y(rx_empty)
);

NAND3X1 _213_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_12_),
    .B(_32_),
    .C(_180_),
    .Y(_33_)
);

DFFSR _442_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_cnt[0]),
    .CLK(rxclk_bF$buf4),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_1_[0])
);

INVX1 _251_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_sample_cnt[1]),
    .Y(_64_)
);

OAI21X1 _307_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_97_),
    .B(_105_),
    .C(_106_),
    .Y(_107_)
);

INVX1 _345_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[7]),
    .Y(_135_)
);

OAI21X1 _383_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_168_),
    .C(_169_),
    .Y(_4_[7])
);

DFFSR _439_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_sample_cnt[1]),
    .CLK(rxclk_bF$buf3),
    .R(_10__bF$buf4),
    .S(vdd),
    .D(_5_[1])
);

INVX1 _192_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_13_),
    .Y(_14_)
);

INVX1 _248_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_44_),
    .Y(_62_)
);

NAND2X1 _286_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf3),
    .B(rx_reg[6]),
    .Y(_89_)
);

NAND2X1 _189_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_busy),
    .B(rx_enable),
    .Y(_11_)
);

BUFX2 _401_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_181_[5]),
    .Y(rx_data[5])
);

OAI21X1 _210_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_25_),
    .B(_29_),
    .C(rx_reg[2]),
    .Y(_31_)
);

NAND2X1 _304_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[1]),
    .B(_93_),
    .Y(_104_)
);

INVX1 _342_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[6]),
    .Y(_133_)
);

NOR2X1 _207_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_15_),
    .B(_16_),
    .Y(_28_)
);

INVX1 _380_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_166_),
    .Y(_167_)
);

DFFSR _436_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[6]),
    .CLK(rxclk_bF$buf3),
    .R(_10__bF$buf4),
    .S(vdd),
    .D(_4_[6])
);

NAND2X1 _245_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_53_),
    .B(_59_),
    .Y(_60_)
);

NAND2X1 _283_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf2),
    .B(rx_reg[5]),
    .Y(_87_)
);

INVX1 _339_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[5]),
    .Y(_131_)
);

OAI21X1 _377_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_149_),
    .B(_162_),
    .C(_164_),
    .Y(_8_)
);

NOR2X1 _186_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[1]),
    .B(rx_cnt[0]),
    .Y(_178_)
);

NOR2X1 _301_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_97_),
    .B(_100_),
    .Y(_101_)
);

NAND2X1 _204_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_12_),
    .B(_180_),
    .Y(_25_)
);

DFFSR _433_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[3]),
    .CLK(rxclk_bF$buf4),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_4_[3])
);

OAI21X1 _242_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_26_),
    .B(_46_),
    .C(_57_),
    .Y(_1_[2])
);

NAND2X1 _280_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(uld_rx_data_bF$buf0),
    .B(rx_reg[4]),
    .Y(_85_)
);

INVX1 _336_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[4]),
    .Y(_129_)
);

OAI21X1 _374_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_152_),
    .B(_156_),
    .C(_161_),
    .Y(_162_)
);

NAND2X1 _239_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_enable),
    .B(_28_),
    .Y(_55_)
);

NAND2X1 _277_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_reg[3]),
    .B(uld_rx_data_bF$buf1),
    .Y(_83_)
);

NAND2X1 _201_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_cnt[2]),
    .B(_22_),
    .Y(_23_)
);

DFFSR _430_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(rx_reg[0]),
    .CLK(rxclk_bF$buf4),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_4_[0])
);

INVX1 _333_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[3]),
    .Y(_127_)
);

AND2X2 _371_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_157_),
    .B(_158_),
    .Y(_159_)
);

DFFSR _427_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[6]),
    .CLK(rxclk_bF$buf4),
    .R(_10__bF$buf1),
    .S(vdd),
    .D(_2_[6])
);

OR2X2 _236_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_179_),
    .B(rx_sample_cnt[3]),
    .Y(_52_)
);

NAND2X1 _274_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_reg[2]),
    .B(uld_rx_data_bF$buf3),
    .Y(_81_)
);

OAI21X1 _368_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_153_),
    .B(_155_),
    .C(tx_cnt[0]),
    .Y(_156_)
);

INVX1 _330_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_data[2]),
    .Y(_125_)
);

DFFSR _424_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[3]),
    .CLK(rxclk_bF$buf1),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_2_[3])
);

OAI21X1 _233_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_16_),
    .B(_46_),
    .C(_50_),
    .Y(_1_[0])
);

NAND2X1 _271_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_reg[1]),
    .B(uld_rx_data_bF$buf2),
    .Y(_79_)
);

INVX1 _327_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[1]),
    .Y(_123_)
);

AOI21X1 _365_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_140_),
    .B(_139_),
    .C(tx_reg[0]),
    .Y(_153_)
);

NAND2X1 _268_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(rx_reg[0]),
    .B(uld_rx_data_bF$buf1),
    .Y(_77_)
);

DFFSR _421_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(_181_[0]),
    .CLK(rxclk_bF$buf0),
    .R(_10__bF$buf5),
    .S(vdd),
    .D(_2_[0])
);

INVX1 _230_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_27_),
    .Y(_48_)
);

AND2X2 _324_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_183_),
    .B(ld_tx_data),
    .Y(_121_)
);

NAND3X1 _362_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_reg[2]),
    .B(_143_),
    .C(_142_),
    .Y(_150_)
);

DFFSR _418_ (
    .gnd(gnd),
    .vdd(vdd),
    .Q(tx_cnt[1]),
    .CLK(txclk),
    .R(_10__bF$buf2),
    .S(vdd),
    .D(_6_[1])
);

OAI21X1 _227_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_42_),
    .B(_180_),
    .C(_44_),
    .Y(_45_)
);

NOR2X1 _265_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(_177_),
    .B(_74_),
    .Y(_75_)
);

AOI22X1 _359_ (
    .gnd(gnd),
    .vdd(vdd),
    .A(tx_cnt[2]),
    .B(_131_),
    .C(_103_),
    .D(_104_),
    .Y(_147_)
);

endmodule
