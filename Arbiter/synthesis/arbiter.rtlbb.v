/* Verilog module written by vlog2Verilog (qflow) */
/* With bit-blasted vectors */
/* With power connections converted to binary 1, 0 */

module arbiter(
    input clk,
    output gnt0,
    output gnt1,
    output gnt2,
    output gnt3,
    input req0,
    input req1,
    input req2,
    input req3,
    input rst
);

wire _60_ ;
wire _19_ ;
wire _57_ ;
wire _16_ ;
wire _54_ ;
wire _13_ ;
wire _51_ ;
wire _7_ ;
wire _48_ ;
wire req0 ;
wire req1 ;
wire req2 ;
wire req3 ;
wire _10_ ;
wire _4_ ;
wire _45_ ;
wire _1_ ;
wire _42_ ;
wire _39_ ;
wire clk ;
wire _36_ ;
wire lmask0 ;
wire lmask1 ;
wire _33_ ;
wire _30_ ;
wire _27_ ;
wire _24_ ;
wire _59_ ;
wire _21_ ;
wire _18_ ;
wire _56_ ;
wire _15_ ;
wire _53_ ;
wire _9_ ;
wire _12_ ;
wire _50_ ;
wire _6_ ;
wire _47_ ;
wire rst ;
wire _3_ ;
wire _44_ ;
wire _0_ ;
wire _41_ ;
wire _38_ ;
wire _35_ ;
wire _32_ ;
wire _29_ ;
wire _26_ ;
wire lasmask ;
wire _23_ ;
wire _58_ ;
wire _20_ ;
wire _17_ ;
wire _55_ ;
wire gnt0 ;
wire gnt1 ;
wire gnt2 ;
wire gnt3 ;
wire _14_ ;
wire _52_ ;
wire _8_ ;
wire _49_ ;
wire _11_ ;
wire _5_ ;
wire _46_ ;
wire ledge ;
wire _2_ ;
wire _43_ ;
wire _40_ ;
wire _37_ ;
wire _34_ ;
wire _31_ ;
wire _28_ ;
wire _25_ ;
wire _22_ ;

OAI22X1 _95_ (
    .A(lmask0),
    .B(_22_),
    .C(req2),
    .D(_38_),
    .Y(_39_)
);

DFFPOSX1 _127_ (
    .Q(_58_),
    .CLK(clk),
    .D(_3_)
);

INVX1 _92_ (
    .A(req1),
    .Y(_36_)
);

DFFPOSX1 _124_ (
    .Q(lasmask),
    .CLK(clk),
    .D(_0_)
);

NOR2X1 _89_ (
    .A(req0),
    .B(req1),
    .Y(_33_)
);

BUFX2 _121_ (
    .A(_60_),
    .Y(gnt3)
);

NAND2X1 _86_ (
    .A(_60_),
    .B(_17_),
    .Y(_30_)
);

BUFX2 _118_ (
    .A(_57_),
    .Y(gnt0)
);

AOI21X1 _83_ (
    .A(_24_),
    .B(_27_),
    .C(req0),
    .Y(_28_)
);

XOR2X1 _115_ (
    .A(lasmask),
    .B(ledge),
    .Y(_55_)
);

NOR2X1 _80_ (
    .A(req3),
    .B(req2),
    .Y(_25_)
);

NOR2X1 _112_ (
    .A(lasmask),
    .B(ledge),
    .Y(_53_)
);

NAND3X1 _77_ (
    .A(lmask1),
    .B(_15_),
    .C(_16_),
    .Y(_22_)
);

AOI21X1 _109_ (
    .A(_50_),
    .B(_46_),
    .C(rst),
    .Y(_2_)
);

NAND3X1 _74_ (
    .A(_8_),
    .B(_15_),
    .C(_16_),
    .Y(_19_)
);

AND2X2 _106_ (
    .A(_25_),
    .B(_36_),
    .Y(_48_)
);

AOI22X1 _71_ (
    .A(_58_),
    .B(req1),
    .C(req0),
    .D(_57_),
    .Y(_16_)
);

AND2X2 _103_ (
    .A(_45_),
    .B(_10_),
    .Y(_4_)
);

OAI21X1 _68_ (
    .A(_58_),
    .B(_11_),
    .C(_10_),
    .Y(_14_)
);

AOI21X1 _100_ (
    .A(_13_),
    .B(req3),
    .C(_22_),
    .Y(_43_)
);

OAI21X1 _65_ (
    .A(_59_),
    .B(_11_),
    .C(_10_),
    .Y(_12_)
);

INVX1 _62_ (
    .A(lasmask),
    .Y(_9_)
);

AOI21X1 _97_ (
    .A(_40_),
    .B(_30_),
    .C(rst),
    .Y(_5_)
);

DFFPOSX1 _129_ (
    .Q(_60_),
    .CLK(clk),
    .D(_5_)
);

OAI21X1 _94_ (
    .A(lmask0),
    .B(_36_),
    .C(_37_),
    .Y(_38_)
);

DFFPOSX1 _126_ (
    .Q(_57_),
    .CLK(clk),
    .D(_2_)
);

NOR2X1 _91_ (
    .A(_34_),
    .B(_31_),
    .Y(_35_)
);

DFFPOSX1 _123_ (
    .Q(lmask1),
    .CLK(clk),
    .D(_7_)
);

INVX1 _88_ (
    .A(req2),
    .Y(_32_)
);

BUFX2 _120_ (
    .A(_59_),
    .Y(gnt2)
);

AOI21X1 _85_ (
    .A(_29_),
    .B(_18_),
    .C(rst),
    .Y(_3_)
);

INVX1 _117_ (
    .A(_56_),
    .Y(_1_)
);

OR2X2 _82_ (
    .A(_26_),
    .B(_17_),
    .Y(_27_)
);

INVX1 _114_ (
    .A(_54_),
    .Y(_0_)
);

NAND3X1 _79_ (
    .A(_13_),
    .B(_21_),
    .C(_23_),
    .Y(_24_)
);

AOI21X1 _111_ (
    .A(_48_),
    .B(_51_),
    .C(_17_),
    .Y(_52_)
);

INVX1 _76_ (
    .A(req3),
    .Y(_21_)
);

OAI21X1 _108_ (
    .A(_49_),
    .B(_47_),
    .C(req0),
    .Y(_50_)
);

NAND2X1 _73_ (
    .A(_58_),
    .B(_17_),
    .Y(_18_)
);

OAI21X1 _105_ (
    .A(_17_),
    .B(_26_),
    .C(_24_),
    .Y(_47_)
);

AOI22X1 _70_ (
    .A(_60_),
    .B(req3),
    .C(_59_),
    .D(req2),
    .Y(_15_)
);

OAI21X1 _102_ (
    .A(_32_),
    .B(_44_),
    .C(_41_),
    .Y(_45_)
);

INVX1 _67_ (
    .A(lmask0),
    .Y(_13_)
);

OR2X2 _64_ (
    .A(_9_),
    .B(_60_),
    .Y(_11_)
);

NAND2X1 _99_ (
    .A(req1),
    .B(_13_),
    .Y(_42_)
);

INVX1 _61_ (
    .A(lmask1),
    .Y(_8_)
);

OAI21X1 _96_ (
    .A(_35_),
    .B(_39_),
    .C(req3),
    .Y(_40_)
);

DFFPOSX1 _128_ (
    .Q(_59_),
    .CLK(clk),
    .D(_4_)
);

INVX1 _93_ (
    .A(_19_),
    .Y(_37_)
);

DFFPOSX1 _125_ (
    .Q(ledge),
    .CLK(clk),
    .D(_1_)
);

NAND2X1 _90_ (
    .A(_32_),
    .B(_33_),
    .Y(_34_)
);

DFFPOSX1 _122_ (
    .Q(lmask0),
    .CLK(clk),
    .D(_6_)
);

NAND2X1 _87_ (
    .A(lmask0),
    .B(_23_),
    .Y(_31_)
);

BUFX2 _119_ (
    .A(_58_),
    .Y(gnt1)
);

OAI21X1 _84_ (
    .A(_20_),
    .B(_28_),
    .C(req1),
    .Y(_29_)
);

NAND2X1 _116_ (
    .A(_55_),
    .B(_52_),
    .Y(_56_)
);

OAI21X1 _81_ (
    .A(lmask1),
    .B(_25_),
    .C(lmask0),
    .Y(_26_)
);

NAND2X1 _113_ (
    .A(_53_),
    .B(_52_),
    .Y(_54_)
);

INVX1 _78_ (
    .A(_22_),
    .Y(_23_)
);

INVX1 _110_ (
    .A(req0),
    .Y(_51_)
);

NOR2X1 _75_ (
    .A(lmask0),
    .B(_19_),
    .Y(_20_)
);

AND2X2 _107_ (
    .A(_20_),
    .B(_48_),
    .Y(_49_)
);

NAND2X1 _72_ (
    .A(_15_),
    .B(_16_),
    .Y(_17_)
);

NAND2X1 _104_ (
    .A(_57_),
    .B(_17_),
    .Y(_46_)
);

AOI21X1 _69_ (
    .A(_9_),
    .B(_13_),
    .C(_14_),
    .Y(_6_)
);

AOI22X1 _101_ (
    .A(_37_),
    .B(_42_),
    .C(_43_),
    .D(_33_),
    .Y(_44_)
);

AOI21X1 _66_ (
    .A(_8_),
    .B(_9_),
    .C(_12_),
    .Y(_7_)
);

INVX1 _63_ (
    .A(rst),
    .Y(_10_)
);

NAND2X1 _98_ (
    .A(_59_),
    .B(_17_),
    .Y(_41_)
);

endmodule
